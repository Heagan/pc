from bytes import read_file_bytes, write_file_bytes
from delta import delta_encoding, delta_decoding
from run_length import run_length_encode, run_length_decode
from boiler import add_boiler_bytes, remove_boiler_bytes

from inverse_delta import inverse_delta_decoding, inverse_delta_encoding

def main():
    f = input("What file would you like to compress?\n> ")

    print('Compressed bytes:')
    c = read_file_bytes(f"{f}")
    original_length = len(c)
    c = delta_encoding(c)
    c = run_length_encode(c)
    c = add_boiler_bytes(c)
    write_file_bytes(f"testfiles/enc_{f[-5:]}", c)

    print(original_length)
    print(len(c))
    print(f"Compression ratio: {round(original_length / len(c), 3)}%")

    print('Decompressed bytes:')
    c = read_file_bytes(f"testfiles/enc_{f[-5:]}")
    c = remove_boiler_bytes(c)
    c = run_length_decode(c)
    c = delta_decoding(c)
    write_file_bytes(f"testfiles/decypt_{f[-5:]}", c)
    
if __name__ == "__main__":
    try:
        main()
    except Exception as e:
        print('Error', e)

