from convert.convert_hx_and_binary import convert_binary_hex, convert_hex_binary
from convert.convert_hx_and_bytes import convert_bytes_hx, convert_hx_bytes

def read_file_bytes(filename):
	with open(filename, 'rb') as f:
		byte_data = f.read()
		hx_data = convert_bytes_hx(byte_data)
		bi = convert_hex_binary(hx_data)
		return bi

def write_file_bytes(filename, binary):
	''' Convert 0's and 1's to a file byte data '''
	hx_data = convert_binary_hex(binary)
	byte_data = convert_hx_bytes(hx_data)

	with open(filename, 'wb+') as f:
		f.write(byte_data)
