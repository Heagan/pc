# Pattern Compressor

I have an idea, want to try it out and see if it'll work
<br>I'm trying to compress files at the cost of all your computers memory, GG

The inspiration for this project was from a video explaining how compression of pokemon sprites where done for the Game Boy
<br>[Here's the video](https://www.youtube.com/watch?v=aF1Yw_wu2cM)

After doing this project, I found a small improvement to compress files
<br>At most I would get a 10% reduction in size
<br>And found that files which already have good compression algorithms where worse
<br>Sometimes this algorithm resulted in the file being a bit larger 

## Method

I've implemented a program that takes advantage of 2 algorithms,
<br>Delta Encoding and Run Length Encoding

I then run these algorithms on the binary data of files because theres more chance of patterns appearing between 1's and 0's rather than hex's 16 different characters,
<br>Though because of theres not much room to store additional data about how it was compressed we're forced to only compress 1 pair of binary data, 00 pairs

## Run Length Encoding

So you want to implement binary Run Length Encoding?
<br>Binary RLE works by taking 00 pairs and replacing them with a counter which says how many 00 pairs where there
<br>The rest of the binary pairs are ignored: 01 10 11

Steps to decode:
- Read the first bit, if its a 0 that means its an RLE packet
- Continue reading until you get a 0, your read bits tell you how many more bits to read
- Take the first of the read bits and add the second read bits, then add 1 
- The result will tell you how many 0 pairs there are
- You will know then that a data packet follows and you can keep reading until you reach the terminator which is a 00
- After that another RLE packet follows

## Delta Encoding

The RLE algorithm works well with 00 pairs and Delta Encoding eliminates sequences of 11 pairs
<br>This is because DE records the difference between data not the data itself,
<br>reducing the comlexety of the data and may help it compress better

If a bit is the same as the one before it, encode it as a 0 else a 1
<br>An imaginary first bit is assumed to be 0 which is used to decode it back

eg.
<br>11111111
<br>Would be saved as
<br>10000000

![](https://www.memesmonkey.com/images/memesmonkey/9e/9e67f18b473683fa8f1363fcc7a5bfbb.png)

