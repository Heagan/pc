

def read_rle_packet(binary, idx):
	num_bytes = ''
	for i in range(idx, len(binary)):
		num_bytes += binary[i]
		if binary[i] == '0':
			idx = i + 1
			break

	num_zeros = ''
	for i in range(idx, idx + len(num_bytes)):
		num_zeros += binary[i]
	idx = idx + len(num_bytes) # Shift idx


	num_zero_pairs = int(num_bytes, 2) + int(num_zeros, 2) + int('1', 2) # this +1 was added in the encoder to fix an error 

	return '00' * num_zero_pairs, idx

def write_rle_packet(zero_pairs):
	zero_pairs += 1 # to make up for issues where zero_pairs == 1, we make it 2
	bi_zero_pairs = bin(zero_pairs)[2:]
	V = bi_zero_pairs[1:]
	L = bin(int(bi_zero_pairs, 2) - int(V, 2) - 2)
	return (L + V)[2:]
