"""

Delta Encoding records the difference between data instead of the data itself

eg.
11111111
would be encoded as
00000000

this is usefull because it promotes pairs of 00's which the RLE works better with

"""

def delta_encoding(binary):
	if binary[0] == '0':
		de = '0'
	else:
		de = '1'

	for i in range(1, len(binary)):
		if binary[i] == binary[i-1]:
			de += '0'
		else:
			de += '1'
	return de

def delta_decoding(de_binary):
	dd = '0'
	for i in range(0, len(de_binary)):
		if de_binary[i] == '0':
			dd += dd[-1]
		else:
			dd += '0' if dd[-1] == '1' else '1'
	return dd[1:]
