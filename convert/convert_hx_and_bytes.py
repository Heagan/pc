from binascii import hexlify, unhexlify

def convert_bytes_hx(bytes):
	return str(hexlify(bytes))[2:-1]

def convert_hx_bytes(hx):
	return unhexlify(hx)
