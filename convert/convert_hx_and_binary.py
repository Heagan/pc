
bi = {
	'0': '0000',
	'1': '0001',
	'2': '0010',
	'3': '0011',
	'4': '0100',
	'5': '0101',
	'6': '0110',
	'7': '0111',
	'8': '1000',
	'9': '1001',
	'a': '1010',
	'b': '1011',
	'c': '1100',
	'd': '1101',
	'e': '1110',
	'f': '1111'
}

def convert_hex_binary(hx):
	binary = ''
	for i in range(0, len(hx)):
		binary += bi[hx[i]]
	return binary


def convert_binary_hex(binary):
	hx = ''
	for i in range(0, len(binary), 4):
		hx += list(bi.keys())[list(bi.values()).index(binary[i:i+4])]
	return hx
