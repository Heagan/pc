
def read_data_packet(binary, idx):
	data = ''
	for i in range(idx, len(binary), 2):
		if binary[i] == '0' and binary[i + 1] == '0':
			idx = i + 2
			break 
		data += binary[i] + binary[i + 1]
	return data, idx
