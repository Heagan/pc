
def add_boiler_bytes(binary):
	n = 8 - len(binary) % 8
	return "0" * (n - 1) + "1" + binary

def remove_boiler_bytes(binary):
	idx = 0
	while idx < len(binary):
		if binary[idx] == '1':
			return binary[idx + 1:] 
		idx += 1
	return binary
