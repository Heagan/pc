"""
Delta Encoding records the difference between data instead of the data itself
But here we will be recording the if data differs from each other

eg.
10101010
would be encoded as
11111111

then using the normal delta encoding will convert that to 00 pairs
we turn this into 1's because there are more 1's in a file usually than 0's
"""

def inverse_delta_encoding(binary):
	if binary[0] == '1':
		de = '1'
	else:
		de = '0'

	for i in range(1, len(binary)):
		if binary[i] == binary[i-1]:
			de += '1'
		else:
			de += '0'
	return de

def inverse_delta_decoding(binary):
	dd = '1'
	for i in range(0, len(binary)):
		
		if binary[i] == '0':
			dd += '0' if dd[-1] == '1' else '1'
		else:
			dd += dd[-1]
	return dd[1:]

# c = "00000000"
# print(c)
# print(inverse_delta_encoding(c))
# print(inverse_delta_decoding(inverse_delta_encoding(c)))
# print()

# c = "11111111"
# print(c)
# print(inverse_delta_encoding(c))
# print(inverse_delta_decoding(inverse_delta_encoding(c)))
# print()

# c = "10101010"
# print(c)
# print(inverse_delta_encoding(c))
# print(inverse_delta_decoding(inverse_delta_encoding(c)))
# print()

