from rle_packets import read_rle_packet, write_rle_packet
from data_packet import read_data_packet

def run_length_encode(binary):
	encoding = ''
	num_zero_pairs = 0
	''' Loop in pairs 2 bits at a time '''
	for i in range(0, len(binary), 2): 

		''' if it a 00 bit pair do RLE '''
		if binary[i] == '0' and binary[i + 1] == '0':
			num_zero_pairs += 1
		else:
			''' we need to save the progress made '''
			if num_zero_pairs > 0: 
				''' 0 to indicate the encoding starts with an RLE packet '''
				if len(encoding) == 0: 
					encoding += '0'
				else: 
					encoding += '00' # terminate the data packet
				rle_packet = write_rle_packet(num_zero_pairs)
				encoding += rle_packet
				''' reset counter, we're working with a data packet from now on '''
				num_zero_pairs = 0 

			''' 1 to indicate the encoding starts with an data packet '''
			if len(encoding) == 0: 
				encoding += '1'
		
			encoding += binary[i] + binary[i + 1]

	if num_zero_pairs > 0: 
		''' 0 to indicate the encoding starts with an RLE packet '''
		if len(encoding) == 0: 
			encoding += '0'
		else: 
			encoding += '00' # terminate the data packet
		rle_packet = write_rle_packet(num_zero_pairs)
		encoding += rle_packet
		''' reset counter, we're working with a data packet from now on '''
		num_zero_pairs = 0 

	encoding += '00'         
	return encoding

def run_length_decode(binary):
	
	if binary[0] == '0': # the beginning should be either 0 or 1
		mode = 'RLE' # RLE Packet
	else:
		mode = 'DP' # Data Packet

	decoded_data = ''
	i = 1
	while i < len(binary):
		if mode == 'RLE':
			rle_packet, new_idx = read_rle_packet(binary, i)
			i = new_idx
			decoded_data += rle_packet
			mode = 'DP'
		elif mode == 'DP':
			data_packet, new_idx = read_data_packet(binary, i)
			i = new_idx
			decoded_data += data_packet
			mode = 'RLE'

	return decoded_data

